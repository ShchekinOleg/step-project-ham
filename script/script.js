"use strict";

//SERVICES SECTION
$(function () {
    $("ul.tabs").on("click", "li:not(.active)", function () {
        $(this)
            .addClass("active")
            .siblings()
            .removeClass("active");
        $("div.section-tabs-content")
            .find("div.tab-content")
            .removeClass("active")
            .eq($(this).index())
            .addClass("active");
    });
});

//AMAZING WORK SECTION
let imageArray = document.getElementsByClassName("work-image-item");
let clicked = 0;

$(function loadMore() {
    $("#loader").hide();
    for (let index = 0; index < 12; index++) {
        $(imageArray[index]).show();
    }
    $("#loadMore").on("click", function (event) {
        clicked++;
        $("#loader").show();
        $("#loadMore").hide();
        if (clicked == 1) {
            setTimeout(function () {
                $("#loader").hide();
                for (let index = 12; index < 24; index++) {
                    $(imageArray[index]).fadeIn("slow");
                }
                $("#loadMore").show();
            }, 1000);
        }
        if (clicked == 2) {
            setTimeout(function () {
                $("#loader").hide();
                for (let index = 24; index < imageArray.length; index++) {
                    $(imageArray[index]).fadeIn("slow");
                }
                $("#loadMore").hide();
            }, 1000);
        }
    });
});

$(function filter() {
    $(".work-section-tab-item").on("click", function () {
        $(".work-image-item").hide();
        $(".work-section-tab-item").removeClass("work-section-tab-item-active");
        $(this).addClass("work-section-tab-item-active");
        imageArray = document.getElementsByClassName($(this).attr("data-type"));
        console.log(imageArray);
        for (let index = 0; index < 12; index++) {
            $(imageArray[index]).show();
        }
        if ($(imageArray).length <= 12) {
            $("#loadMore").hide();
        } else {
            $("#loadMore").show();
        }
    });
});

//CAROUSEL
let member = 0;
$(function carousel() {
    $("#sliderList li:first-child")
        .addClass("active")
        .animate({bottom: +12 + "px"}, 300);

    function moveTo(member) {
        $("#sliderList li")
            .removeClass("active")
            .eq(member)
            .addClass("active");
        $("#sliderList li.active").animate({bottom: +12 + "px"}, 300);
        $(".member-list").animate({left: -163 * member + "px"}, 300);
    }

    $("#leftButton").click(function () {
        member = $("#sliderList li.active").index();
        if (member == 0) {
            member = 4;
        }
        member -= 1;
        moveTo(member);
    });

    $("#rightButton").click(function () {
        member = $("#sliderList li.active").index();
        if (member == 3) {
            member = -1;
        }
        member += 1;
        moveTo(member);
    });

    $("#sliderList li").click(function () {
        member = $(this).index();
        moveTo(member);
    });
});

// Mansonry
var $grid = $(".grid").masonry({
    itemSelector: ".grid-item",
    percentPosition: true,
    columnWidth: ".grid-sizer",
});

$grid.imagesLoaded().progress(function () {
    $grid.masonry();
});

let elements = $(".grid-item");
var $items = Array.from(elements);
$(function masonryLoadmore() {
    $("#masonryLoader").hide();
    for (let i = 1; i < 8; i++) {
        $($items[i]).show();
    }
    $("#masonryLoadMore").on("click", function () {
        $("#masonryLoadMore").hide();
        $(".grid").css("height", "3000px");
        $("#masonryLoader").hide();
        for (let i = 8; i < 14; i++) {
            $($items[i]).show();
        }
    });
});
